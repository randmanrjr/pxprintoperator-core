/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'utils/auth', 'utils/printers', 'utils/messages','utils/messagestrings', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
  'ojs/ojoffcanvas', 'ojs/ojmodel'],
  function(oj, ko, authUtils, printUtils, msg, msgStrings) {
     function ControllerViewModel() {
       var self = this;

      // Media queries for repsonsive layouts
      var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
      var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
      self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);

       // Router setup
       self.router = oj.Router.rootInstance;
       self.router.configure({
         'dashboard': {label: 'Dashboard'},
         'print'    : {label: 'Print Labels', isDefault: true},
         'jobstatus': {label: 'Job Status'}
       });
      oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

      // Navigation setup
      var navData = [
      {name: 'Print Labels', id: 'print',
       iconClass: 'oj-navigationlist-item-icon fa fa-print'},
      {name: 'Job Status', id: 'jobstatus',
       iconClass: 'oj-navigationlist-item-icon fa fa-check-circle-o'},
      {name: 'Dashboard', id: 'dashboard',
       iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'},

      ];
      self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

      // Drawer
      // Close offcanvas on medium and larger screens
      self.mdScreen.subscribe(function() {oj.OffcanvasUtils.close(self.drawerParams);});
      self.drawerParams = {
        displayMode: 'push',
        selector: '#navDrawer',
        content: '#pageContent'
      };
      // Called by navigation drawer toggle button and after selection of nav drawer item
      self.toggleDrawer = function() {
        return oj.OffcanvasUtils.toggle(self.drawerParams);
      };
      // Add a close listener so we can move focus back to the toggle button when the drawer closes
      $("#navDrawer").on("ojclose", function() { $('#drawerToggleButton').focus(); });

      // Header
      // Application Name used in Branding Area
      self.appName = ko.observable("Pragmatyxs LaaS");
      // global messages observable array
      self.globalMessages = ko.observableArray();
      self.messageStrings = new msgStrings.MessageStrings();
      //job status
      self.jobStatusList = ko.observableArray(null);
      self.jobStatusListFormatted = ko.observableArray(null);
      self.jobStatusPopulated=ko.observable(false);

         //create and set the user profile
         self.userProfile = ko.observable(new authUtils.UserProfile());
         self.userProfile().getUserProfile();

         //check if user profile is valid
         self.userProfile().userProfileValid.subscribe(function () {
             if (self.userProfile().userProfileValid() === false) {
                 var message = new msg.Message(
                     self.messageStrings.incompleteUserProfile,
                     'warn',
                     new Date(),
                     -1);
                 self.globalMessages.push(message);
                 message.broadcast();
             }
         });

         self.userLogin = ko.computed(function () {
             var userP = ko.toJS(self.userProfile);
             return userP.user.userFullName;
         });

         // print context
         self.printContext = ko.computed(function () {
             var userP = ko.toJS(self.userProfile);
             return ko.toJS(userP.printContext);
         });

         //printer list
         self.printerList = ko.observableArray();

         //promise to get the list of printers
         self.getPrinterList = new Promise(function (resolve) {
             //recursive function to wait until the printContext is set...
             (function waitForPrintContext() {
                 if (self.printContext().site && self.printContext().location) return resolve();
                 setTimeout(waitForPrintContext, 30);
             }())
         }).then(function () {
             // get the list of printers via the printUtils
             printUtils.getPrinterList(self.printContext(), function (data) {
                 var message;
                 if ((data === 'undefined') || (data === 'Unauthorized') || (data === 'ERROR') || (data === 'Not Found')) {
                     self.printerList([self.messageStrings.printerListFailure]);
                     //display warning message to user
                     message = new msg.Message(self.messageStrings.printerListFailure, 'warn', new Date(), -1 );
                     self.globalMessages.push(message);
                     message.broadcast();
                 } else {
                     try {
                         var _datap = JSON.parse(data);
                         self.printerList(_datap);
                         if (self.printerList().length > 0) {
                             message = new msg.Message(self.messageStrings.printerListSuccess, 'success', new Date(), 4 );
                             self.globalMessages.push(message);
                             message.broadcast();
                         } else {
                             message = new msg.Message(self.messageStrings.noPrintersForProfile, 'warn', new Date(), -1 );
                             self.globalMessages.push(message);
                             message.broadcast();
                         }

                     } catch (ex) {
                         console.error('data does not parse as json:', ex);
                         message = new msg.Message(self.messageStrings.printerListFailure, 'warn', new Date(), -1 );
                         self.globalMessages.push(message);
                         message.broadcast();
                     }
                 }
             });
         });

         // Footer
      function FooterLink(name, id, linkTarget) {
        this.name = name;
        this.linkId = id;
        this.linkTarget = linkTarget;
      }
      self.footerLinks = ko.observableArray([
        new FooterLink('About Pragmatyxs', 'aboutPragmatyxs', 'https://www.pragmatyxs.com/about/'),
        new FooterLink('Contact Us', 'contactUs', 'https://www.pragmatyxs.com/contact/')
      ]);

         self.userMenuSelect = function (event, ui) {
             var elmen = ui.item.attr("id");
             if (elmen === 'out') {
                 window.location = '/logout';
             }
         };

     } //end ControllerViewModel

     return new ControllerViewModel();
  }
);
