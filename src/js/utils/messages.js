// Global Messages Module; returns an instance of the Message Class

define(['knockout','moment','promise'], function (ko,moment) {

    //Message Class Constructor
    function Message(body, state, time, dur) {
        this.body = typeof body === 'string' ? body : 'Invalid message string';
        this.state = state || 'default';
        this.time =  time;
        this.dur =  ko.observable(dur);
        this.guid = 'msg' + moment().valueOf(); //guid for message

        var that = this;

        this.broadcast = function () {

            //var that = this;

            return new Promise(function(resolve) {
                resolve();
            }).then(function() {
                return endBroadcast();
            });
        };

        // cancels/closes message
        this.cancelMessage = function () {
            that.dur(0)
        };

        function endBroadcast() {
            var delay = function (t) {
                return new Promise(function(resolve) {
                    if (t > 0) { //message auto-cancels after timeout
                        setTimeout(resolve, t*1000);
                    } else { //message cancels only after user input, pass a negative number as the duration
                        resolve();
                    }

                })
            };

            return new Promise(function (resolve) {
                resolve();
            }).then(function () {
                return delay(that.dur()).then(function () {
                    if (that.dur() > 0) {
                        that.dur(0);
                        return that;
                    } else {
                        return that;
                    }
                });
            });
        }
    }

    return {

        Message : Message
    }

});