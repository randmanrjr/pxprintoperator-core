// Print Label Module
define(['knockout','jquery','utils/label','moment','utils/messagestrings','moment-timezone', 'promise'], function (ko,$, lbl, moment, msgStrings) {

    //message strings
    var messageStrings = new msgStrings.MessageStrings();

    //PrintLabel Class Constructor
    function PrintLabelObject(){
        var model = {
            jobName:                ko.observable(''),
            reference:              ko.observable(''),
            printContext:           ko.observable(''),
            labels:                 ko.observableArray()

        };
        this.Print = ko.observable(model);
    }

    // Class Constructor -- public
    function PrintJobCreation(){
        this.PrintList = ko.observable();
        this.apiPrintResponse = ko.observable(null);
        this.PrintHistoryData = {};
        var that=this;

        this.prepareAndPrintLabels = function(labelTypeData, plrData, pcontext, lot){       //labelTypeData = LabelType to be printed including selections for printer, etc.,
                                                                                            // plrData = product label record data used in calculations and contains selected variant and
                                                                                            //country of origin, pcontext = print context, lot = lot selected by user
            return new Promise(function (resolve, reject) {

                //Set Values
                var newprintjob = new PrintLabelObject();
                newprintjob.Print().jobName(labelTypeData.labelType + "|" + plrData.productCode() + "|" + lot + "|" + getCurrentTimestamp());
                newprintjob.Print().reference(pcontext.organization + "-" + pcontext.site + "-" + pcontext.printOperator);
                newprintjob.Print().printContext(pcontext);
                var indvlabel = new lbl.LabelCreation(labelTypeData, plrData, pcontext, lot);
                newprintjob.Print().labels.push(indvlabel);

                //push print job to the list
                that.PrintList(newprintjob);

                //increment to facilitate break out of waitForLabelDataCreation()
                var labelDataTimeoutInc = 0;

                //recursive function to wait until label object(s) are populated
                (function waitForLabelDataCreation() {
                    if ( labelDataTimeoutInc < 800) { //break out after 20s if no label data is created by label.js module
                        if (newprintjob.Print().labels().length>0) return resolve(that.PrintList());
                        setTimeout(waitForLabelDataCreation, 25);
                        labelDataTimeoutInc++;
                    } else {
                        return reject;
                    }

                }())


            }).then(function (data) { //TODO: review, passing in data here may not be necessary, data is not being used!
                return new Promise(function (resolve, reject) {
                    var labelArray = [];
                    console.log('PrintList.Print.Labels[0].LabelSet', that.PrintList().Print().labels()[0].LabelSet());
                    var printrec = that.PrintList().Print().labels()[0].LabelSet();

                    var newprintrec = {
                        format: printrec.format(),
                        quantity: ko.utils.unwrapObservable(printrec.quantity()),
                        duplicates: 0,
                        labelData: printrec.labelData()
                    };
                    labelArray.push(newprintrec);
                    console.log('printrec',printrec);
                    var _data = {
                        jobName: that.PrintList().Print().jobName(),
                        reference: that.PrintList().Print().reference(),
                        printContext: {
                            application: pcontext.application,
                            originatingHost: pcontext.originatingHost,
                            printOperator: pcontext.printOperator,
                            organization: pcontext.organization,
                            site: pcontext.site,
                            location: pcontext.location
                        },
                        labels: labelArray,
                        printer: labelTypeData.printer(),
                        dom: printrec.dom(),
                        expiryDate: printrec.expDate()
                    };

                    PrintHistoryData = _data;

                    var printrequest = $.ajax({
                        type: "POST",
                        url: "/api/v1/labels",
                        data: JSON.stringify(_data),
                        cache: false,
                        contentType: 'application/json',
                        dataType: 'json'
                    });
                    printrequest.done(function (data, textStatus, jqXHR) {
                        //send request body data to next then function for writing to print history
                        resolve(jqXHR);
                    });
                    printrequest.fail(function (jqXHR, exception) {
                        reject(jqXHR);
                    });
                    printrequest.always(function () {

                    });
                }).then (function (data){
                    return new Promise(function (resolve, reject) {
                    //Make API Call to record print history
                    var historyDetails=[];
                    historyDetails.push({"printHistoryID":null,"labelTypeID":labelTypeData.labelTypeKey,"printer":labelTypeData.printer(),"qtyPrinted":labelTypeData.quantity(),
                        "extraCopies":0,"printStatus":'Printed',"jobID":data.jobId});
                    var historyData ={
                        productCode:plrData.productCode(),
                        plrID:plrData.plrId(),
                        productIdentifier:plrData.productIdentifer(),
                        lotNumber:lot,
                        manufactureDate:moment(that.PrintList().Print().labels()[0].LabelSet().dom()).format("DD-MMM-YYYY"),
                        countryOfOriginID:  getCountryOfOriginId(plrData.countriesOfOrigin(), plrData.countryOfOrigin()),
                        printUser:pcontext.printOperator,
                        organization:pcontext.organization,
                        lotQty:labelTypeData.quantity(),
                        historyDetails:historyDetails,
                        isReprint:plrData.isReprint(),
                        jobID:data.jobId,
                        batchID:plrData.batchId()
                    };
                    if (PrintHistoryData.expiryDate) {
                        historyData["expirationDate"] = moment(that.PrintList().Print().labels()[0].LabelSet().expDate()).format("DD-MMM-YYYY");
                    }   else{
                            historyData["expirationDate"] = null;
                        }

                    if (plrData.variant()){
                        historyData["variant"] = plrData.variant();
                    } else{
                        historyData["variant"] = null;
                    }
                    console.log('historyData',historyData);
                    var historyrequest = $.ajax({
                        type: "POST",
                        url: "/oracledb/api/v1/printhistory",
                        data: JSON.stringify(historyData),
                        cache: false,
                        contentType: 'application/json',
                        dataType: 'json'
                    });
                    historyrequest.done(function (data, textStatus, jqXHR) {
                        resolve({jqXHR:jqXHR, historyData: historyData});
                    });
                    historyrequest.fail(function (jqXHR, exception) {
                        console.log('history request done: reached fail',exception);
                        reject({jqXHR:jqXHR, historyData:historyData});
                    });
                    historyrequest.always(function () {

                    });

                    }).then(function (data){
                        //Then function after making API call to record print history (promise is to be resolved)
                        console.log('Print and Print History Insert is complete -- SUCCESS',data.jqXHR);
                        var returnResponse = {responseCode:data.jqXHR.status,responseText:'Submitted',responseType:'success',responseDetails:data.jqXHR, responseSource:'Print', historyData:data.historyData};
                        that.apiPrintResponse(returnResponse);
                        return returnResponse;
                    }).catch(function(data){
                        //Catch from API Call to record print history
                        console.log('Print History Insert is complete -- FAIL',data.jqXHR);
                        var returnResponse = {responseCode:data.jqXHR.status,responseText:data.jqXHR.statusText,responseType:'alert',responseDetails:data.jqXHR, responseSource:'History',historyData:data.historyData};
                        that.apiPrintResponse(returnResponse);
                        return returnResponse;
                    })
                }).catch(function(jqXHR){
                    //Catch for API Call to Print Label
                    var returnResponse = {responseCode:jqXHR.status,responseText:jqXHR.statusText,responseType:'alert',responseDetails:jqXHR, responseSource:'Print'};
                    that.apiPrintResponse(returnResponse);
                    return returnResponse;
                })
            }).catch(function () {
                //Catch from generating print data
                console.error('catch function reached...');
                var returnResponse = {responseCode:'error',responseText:messageStrings.labelDataFailure,responseType:'alert',responseDetails:null, responseSource:'Print'};
                that.apiPrintResponse(returnResponse);
                return returnResponse;
            })


        };

        function getCurrentTimestamp(){

            moment.tz.setDefault("America/New_York");
            var dt= moment();
            return moment(dt).format("YYYY-MM-DD HH:mm:ss");
        }

        function getCountryOfOriginId(COOArray, COOValue){
            var cooRecord = COOArray.filter(function(coo){return coo.name == COOValue});
            return cooRecord[0].value;
        }

        return that.PrintList();
    }

    return {

        PrintJobCreation: PrintJobCreation
    }

});