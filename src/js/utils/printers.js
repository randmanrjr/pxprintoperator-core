// Get Available Printers Module

define(['jquery'], function ($) {

    var getPrinterList = function (pc, callback) { //pc = printContext callback = callback
        $.ajax({
            type: "POST",
            url: "/api/v1/printers",
            data: pc,
            cache: false,
            success: function (d) {
                callback(d)
            },
            error: function (req, status, err) {
                console.log('ajax error:');
                console.log('status:', status);
                if (err) {
                    console.log('error:', err);
                }
            }
        });
    };

    return {

        getPrinterList: getPrinterList
    }

});