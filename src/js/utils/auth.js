// Authorization Utilities Module

define(['knockout','jquery', 'promise'], function (ko,$) {

    //UserProfile Class Constructor
    function UserProfile() {
        //user profile data model
        var userModel = {
            names:              ko.observable(),
            userFullName:       ko.observable(),
            userName:           ko.observable(),
            role:               ko.observable()
        };
        //print context data model
        var pContextModel = {
            originatingHost:    ko.observable(window.location.hostname),
            application:        ko.observable('pxprintoperator'),
            organization:       ko.observable(),
            site:               ko.observable(),
            location:           ko.observable(),
            printOperator:      ko.observable(),
            userID:             ko.observable(),
            role:               ko.observable()
        };

        this.user = ko.observable(userModel);
        this.printContext = ko.observable(pContextModel);
        this.userProfileValid = ko.observable();

        var that = this;

        this.getUserProfile = function () {

            $.ajax({
                type: "POST",
                url: "/getuser",
                data: null,
                dataType: "text",
                success: function (d) {
                    return setUserCallback(d)
                },
                error: function (req, status, err) {
                    console.log('ajax error:');
                    console.log('status:', status);
                    if (err) {
                        console.log('error:', err);
                    }
                }
            })

        };

        var setUserCallback = function (data) {
            return new Promise(function (resolve, reject) {
                //try to parse data returned from ajax
                try {
                    var dp = JSON.parse(data);
                    var dpUser = JSON.parse(dp.user);
                } catch (err) {
                    console.error(err);
                    //call the reject callback
                    reject('profile data does not parse as json');
                }
                // check user profile to ensure no undefined properties
                if ( typeof (dpUser.groups && dpUser.name.givenName && dpUser.name.familyName && dpUser.name.honorificPrefix && dpUser.name.honorificSuffix) !== 'undefined') {
                    that.userProfileValid(true); //user profile is valid
                    // parse valid profile and make assignments
                    resolve(dpUser);
                } else {
                    that.userProfileValid(false); //user profile is incomplete === invalid
                    //parse invalid profile and make available assignments
                    resolve(dpUser);
                }
            }).then(function (data) {
                //set user profile and print context

                //set user userLogin
                that.user().names(data.name); //sets the names ko.observable as name obj
                //sets the userName (what is displayed in the app)
                if (typeof (data.name.givenName && data.name.familyName) !== ('undefined' || null)) {
                    that.user().userFullName = ko.computed(function () {
                        return data.name.givenName + ' ' + data.name.familyName
                    });
                } else if (typeof (data.name.givenName || data.name.familyName) === ('undefined' || null)) {
                    //set userFullName as either first or last name
                    data.name.givenName ? that.user().userFullName(data.name.givenName) : that.user().userFullName(data.name.familyName)
                } else {
                    // set userFullName as Full Name Missing in profile
                    that.user().userFullName('Full Name Missing in profile')
                }

                //set userName (email address)
                if (typeof data.userName !== ('undefined' || null)) {
                    that.user().userName(data.userName)
                } else {
                    that.user().userName('undefined email')
                }


                var _groups = data.groups;
                //set user role
                _groups.forEach(function (g) {
                    var gp = $.trim(g.display.toString());
                    switch (gp) {
                        case 'PrintAdministrators' : //future case for PrintAdministrators role
                            that.printContext().role(gp);
                            that.user.role(gp);
                            break;
                        case 'PrintOperators' :
                            that.printContext().role(gp);
                            that.user().role(gp);
                            break;
                        default:
                            break;
                    }
                });
                if (that.user().role() === ('undefined' || null)) { that.user().role('undefined role'); that.printContext().role('undefined role');}

                //set organization
                _groups.forEach(function (go) {
                    var gop = $.trim(go.display.toString());
                    switch (gop) {
                        case 'Pragmatyxs' :
                            that.printContext().organization(gop);
                            break;
                        case 'DePuy' :
                            that.printContext().organization(gop);
                            break;
                        default:
                            break;
                    }
                });
                if (that.printContext().organization() === ('undefined' || null)) {that.printContext().organization('undefined organization');}

                //set remainder of print context
                if (typeof data.name.honorificPrefix !== ('undefined' || null)) {
                    that.printContext().site(data.name.honorificPrefix)
                } else {
                    that.printContext().site('undefined site')
                }
                if (typeof data.name.honorificSuffix !== ('undefined' || null)) {
                    that.printContext().location(data.name.honorificSuffix)
                } else {
                    that.printContext().location('undefined location')
                }
                if (typeof (data.name.givenName && data.name.familyName) !== ('undefined' || null)) {
                    that.printContext().printOperator(data.name.givenName + '_' + data.name.familyName);
                } else if (typeof (data.name.givenName || data.name.familyName) === ('undefined' || null)) {
                    data.name.givenName ? that.printContext().printOperator(data.name.givenName) : that.printContext().printOperator(data.name.familyName)
                } else {
                    that.printContext().printOperator('Unidentified_Print_Operator')
                }

                // user id will always be returned by idcs
                that.printContext().userID(data.id);

            }).catch(function (data) {
                //log the error message to the console
                console.log(data);
                that.userProfileValid(false);
            })

        }
    }

    return {

        UserProfile: UserProfile

    }

});