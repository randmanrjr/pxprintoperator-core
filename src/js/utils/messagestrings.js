// Global Message Strings Module
//TODO: Review message strings and make customer agnostic
define([], function () {

    function MessageStrings() {

        //json parsing errors
        this.jsonNoParse =                  'Returned data did not parse as JSON';

        //user profile messages
        this.incompleteUserProfile =        'Your user profile is incomplete, please contact Pragmatyxs Support at: 1-800-419-5097';

        //printer list messages
        this.printerListSuccess =           'Available printers successfully retrieved';
        this.noPrintersForProfile =         'No available printers for this user profile!';
        this.printerListFailure =           'Unable to get available printers!';

        //product data messages
        this.noConnectionLabelData =        'Unable to connect to Product Label Data Source';
        this.timeoutConnectionLableData =   'Product Label Data Source took too long to respond';
        this.noParseConnectionLabelData =   'Unable to process response from Product Label Data Source';
        this.connectionErrorLabelData =     'Unknown error connecting to Product Label Data Source';
        this.problemPartLot =               'There was a problem retrieving product label data for this part/lot';
        this.noProductLabelRecord =         'Product Labels cannot be found for Product / Lot (or they are not approved)';

        //label data messages
        this.labelDataFailure =             'Ohh Snap, something went wrong creating the label data!';

        //print history messages
        this.noPrintHistory =               'Unable to query print history for this part/lot';
        this.noConnectionPrintHistory =     'Unable to connect to Print History Data Source';
        this.timeoutConnectionPrintHistory ='Print History Data Source took too long to respond';
        this.noParseConnectionPrintHistory ='Unable to process response from Print History Data Source';
        this.connectionErrorPrintHistory =  'Unknown error connecting to Print History Data Source';

        //print label messages
        this.printJobSuccess =              'Print Job Successfully Submitted';
        this.printJobFailure =              'Print Job Unsuccessful';
        this.printHistoryWriteSuccess =     'Writing Print History Successful';
        this.printHistoryWriteFailure =     'Writing Print History Unsuccessful';
        this.printJobTimeout =                 'Print Job Timeout';

        //job status messages
        this.jobStatusListFailure =         'Unable to retrieve job status list';

        //print-time validation errors
        //TODO: add print time validation errors??
    }

    return {
        MessageStrings : MessageStrings
    }

});