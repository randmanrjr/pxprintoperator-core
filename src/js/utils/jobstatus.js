//get Job Status List Module

define(['jquery'], function ($) {

    var getJobStatusList = function (pc, callback) { //pc = printContext callback = callback
        var _data = {"organization":pc.organization,"operator":pc.printOperator};
        $.ajax({
            type: "POST",
            url: "/api/v1/jobs",
            data: _data,
            cache: false,
            success: function (d) {
                callback(d)
            },
            error: function (req, status, err) {
                console.log('ajax error:');
                console.log('status:', status);
                if (err) {
                    console.log('error:', err);
                }
            }
        });
    };

    return {

        getJobStatusList: getJobStatusList
    }

});