/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery','utils/messages','utils/jobstatus'],
 function(oj, ko, $, msg, jobStatusUtils) {
  
    function IncidentsViewModel() {
      var self = this;


        var ControllerViewModel =  ko.dataFor(document.getElementById('globalBody'));
        // test ability to get printer list
        self.printerList = ControllerViewModel.printerList;
        self.printContext = ControllerViewModel.printContext;
        self.userProfile = ControllerViewModel.userProfile;
        self.globalMessages = ControllerViewModel.globalMessages;
        self.messageStrings = ControllerViewModel.messageStrings;
        self.jobStatusList = ControllerViewModel.jobStatusList;
        self.jobStatusListFormatted = ControllerViewModel.jobStatusListFormatted;
        self.jobStatusPopulated= ControllerViewModel.jobStatusPopulated;

        self.getJobStatusList = function(){
            //Make Call to get list of Job Statuses
            jobStatusUtils.getJobStatusList(self.printContext(),function(data){
                var message;
                if ((data === 'undefined') || (data === 'Unauthorized') || (data === 'ERROR') || (data === 'Not Found')) {
                    self.jobStatusList([self.messageStrings.jobStatusListFailure]);
                    message = new msg.Message(self.messageStrings.jobStatusListFailure, 'warn', new Date(), -1 );
                    self.globalMessages.push(message);
                    message.broadcast();
                } else {
                    try {
                        var _datap = JSON.parse(data);
                        self.jobStatusList(_datap);

                        //Wait for data to populate and be parsed as JSON
                        var jobStatusResponseTimeoutInc = 0;
                        (function waitForJobStatusResponse() {
                            if (jobStatusResponseTimeoutInc < 400) { //break out after 10s
                                if (ko.utils.unwrapObservable(self.jobStatusList()[0].jobName) !== null) {
                                    return self.jobStatusList();
                                }
                                jobStatusResponseTimeoutInc++;
                                setTimeout(waitForJobStatusResponse,30);
                            }
                        }());

                        var jsl = [];
                        console.log('jobStatusList',self.jobStatusList());
                        self.jobStatusList().forEach(function(job_item){
                            var result = job_item.jobName.split('|');
                            var product='';
                            var lot='';
                            var submitDate = '';
                            var jobNameCount = result.length;
                            if (jobNameCount>1){
                                product=result[1];
                                lot=result[2];
                                submitDate = result[3];
                            }

                            //If jobNameCount>1, the print status record is of the new type with the pipe separator

                            var jslitem = {"Quantity":job_item.quantity,"Printer":job_item.printer,"LabelType":result[0],"Product":product,"Lot":lot, "SubmitDate":submitDate,"Status":job_item.status};
                            jsl.push(jslitem);
                        });
                        self.jobStatusListFormatted(jsl);
                        self.jobStatusPopulated(true);

                    } catch (ex) {
                        console.error('data does not parse as json:', ex);
                        message = new msg.Message(self.messageStrings.jobStatusListFailure, 'warn', new Date(), -1 );
                        self.globalMessages.push(message);
                        message.broadcast();
                    }
                }
            });
        };

      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additionaly available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
      self.handleActivated = function(info) {
        // Implement if needed
          self.getJobStatusList();
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
      self.handleAttached = function(info) {
        // Implement if needed
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
      self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
      self.handleDetached = function(info) {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constrcuted
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new IncidentsViewModel();
  }
);
